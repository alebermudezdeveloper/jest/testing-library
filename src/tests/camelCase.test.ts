import { toCamelCase } from '../core/camelCase'

describe('Camel case converter', () => {
  it('allows empty text', () => {
    expect(toCamelCase('')).toBe('');
  });
  it('allows capitalized word', () => {
    expect(toCamelCase('Foo')).toBe('Foo');
  });
  it('joins the capitalized words that are separated by spaces', () => {
    expect(toCamelCase('Foo Bar')).toBe('FooBar');
  });
  it('joins the capitalized words that are separated by hyphens', () => {
    expect(toCamelCase('Foo-Bar_Foo')).toBe('FooBarFoo');
  });
  it('converts the first character of one word to uppercase', () => {
    expect(toCamelCase('foo')).toBe('Foo');
  });
  it('converts the first character of each word to uppercase', () => {
    expect(toCamelCase('foo-bar_foo')).toBe('FooBarFoo');
  });
})