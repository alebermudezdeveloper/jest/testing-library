import {average, sum} from "../stats";
import * as statsAsync from "../statsAsync";

describe("Stats Async should", ()=> {
  it("calculates the sum of all elements of the array (async)", async () => {
    const result = await sum([1, 2, 3]);
    const expected = 6;
 
    expect(result).toBe(expected);
  });
  
  it("calculates the average of all elements of the array (async)", async () => {
    const result = await average([1, 2, 3]);
    const expected = 2;
 
    expect(result).toBe(expected);
  });
});