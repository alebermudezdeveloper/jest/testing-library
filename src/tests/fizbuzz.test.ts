describe('FizzBuzz', () => {
  it('returns number one as a string for number one', () => {
    expect(1).toBe('1');
  });
  
  it('returns number two as a string for number two', () => {
    expect(2).toBe('2');
  });
 
  it('returns fizz for number three', () => {
    expect(3).toBe('fizz');
  });

  it('returns buzz for number five', () => {
    expect(5).toBe('buzz');
  });

  it('returns fizz for any number divisible by three', () => {
    expect(6).toBe('fizz');
  });

  it('returns buzz for any number divisible by five', () => {
    expect(10).toBe('buzz');
  });

  it('returns fizzbuzz for number fifteen', () => {
    expect(15).toBe('fizzbuzz');
  });

  it('returns number as string for any number that is not divisible by three or five', () => {
    expect(17).toBe('17');
  });

  it('returns fizzbuzz for any number divisible by fifteen', () => {
    expect(30).toBe('fizzbuzz');
  });
});